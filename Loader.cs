﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace Cam_Watcher
{
    public partial class Loader : Form
    {
        FilterInfoCollection webcams;

        public Loader()
        {
            InitializeComponent();
            webcams = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo webcam in webcams)
            {
                flpnl_webcams.Controls.Add(new CamItem(webcam));
            }
        }

        private void buttn_continue_Click(object sender, EventArgs e)
        {
            List<FilterInfo> webcamsList = new List<FilterInfo>();
            foreach (CamItem cam in flpnl_webcams.Controls)
            {
                if (cam.enabled)
                {
                    webcamsList.Add(cam.webcamInfo);
                }
            }
            if (webcamsList.Count > 0)
            {
                Home frmHome = new Home(webcamsList);
                frmHome.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("No webcams selected", "Cam Watcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
