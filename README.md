Released at: April 2020

Description: Monitor your surroundings through multiple webcams and accessing from a website.

XAMPP is required. Must exist the path: C:/xampp/htdocs/cam_watcher/snapshots
Config file struct:
    Line 1: Server path
    Line 2: Current IP
    Line 3: Server URL
    Line 4: Snapshots store path
    Line 5: Seconds to take a snapshot
    Line 6: Sensibility for motion option
