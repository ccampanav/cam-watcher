﻿namespace Cam_Watcher
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.flpnl_list_webcam = new System.Windows.Forms.FlowLayoutPanel();
            this.labl_main_webcam = new System.Windows.Forms.Label();
            this.timer_record = new System.Windows.Forms.Timer(this.components);
            this.labl_time_elapsed = new System.Windows.Forms.Label();
            this.buttn_settings = new System.Windows.Forms.Button();
            this.picbx_spinner = new System.Windows.Forms.PictureBox();
            this.picbx_record = new System.Windows.Forms.PictureBox();
            this.picbx_main_webcam = new System.Windows.Forms.PictureBox();
            this.grpbx_recording = new System.Windows.Forms.GroupBox();
            this.rdbtn_motion = new System.Windows.Forms.RadioButton();
            this.rdbtn_traditional = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.numud_duration = new System.Windows.Forms.NumericUpDown();
            this.labl_duration = new System.Windows.Forms.Label();
            this.timer_server = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picbx_spinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_record)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_main_webcam)).BeginInit();
            this.grpbx_recording.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numud_duration)).BeginInit();
            this.SuspendLayout();
            // 
            // flpnl_list_webcam
            // 
            this.flpnl_list_webcam.AutoScroll = true;
            this.flpnl_list_webcam.Location = new System.Drawing.Point(10, 10);
            this.flpnl_list_webcam.Name = "flpnl_list_webcam";
            this.flpnl_list_webcam.Size = new System.Drawing.Size(270, 555);
            this.flpnl_list_webcam.TabIndex = 4;
            // 
            // labl_main_webcam
            // 
            this.labl_main_webcam.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labl_main_webcam.Location = new System.Drawing.Point(383, 9);
            this.labl_main_webcam.Name = "labl_main_webcam";
            this.labl_main_webcam.Size = new System.Drawing.Size(500, 30);
            this.labl_main_webcam.TabIndex = 6;
            this.labl_main_webcam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer_record
            // 
            this.timer_record.Interval = 1000;
            this.timer_record.Tick += new System.EventHandler(this.timer_record_Tick);
            // 
            // labl_time_elapsed
            // 
            this.labl_time_elapsed.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labl_time_elapsed.Location = new System.Drawing.Point(825, 571);
            this.labl_time_elapsed.Name = "labl_time_elapsed";
            this.labl_time_elapsed.Size = new System.Drawing.Size(155, 50);
            this.labl_time_elapsed.TabIndex = 16;
            this.labl_time_elapsed.Text = "00:00:00";
            this.labl_time_elapsed.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttn_settings
            // 
            this.buttn_settings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttn_settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttn_settings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(206)))), ((int)(((byte)(120)))));
            this.buttn_settings.Location = new System.Drawing.Point(906, 7);
            this.buttn_settings.Name = "buttn_settings";
            this.buttn_settings.Size = new System.Drawing.Size(77, 32);
            this.buttn_settings.TabIndex = 21;
            this.buttn_settings.Text = "Settings";
            this.buttn_settings.UseVisualStyleBackColor = true;
            this.buttn_settings.Click += new System.EventHandler(this.buttn_settings_Click);
            // 
            // picbx_spinner
            // 
            this.picbx_spinner.Image = global::Cam_Watcher.Properties.Resources.spinner;
            this.picbx_spinner.Location = new System.Drawing.Point(848, 621);
            this.picbx_spinner.Name = "picbx_spinner";
            this.picbx_spinner.Size = new System.Drawing.Size(109, 28);
            this.picbx_spinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_spinner.TabIndex = 14;
            this.picbx_spinner.TabStop = false;
            this.picbx_spinner.Visible = false;
            // 
            // picbx_record
            // 
            this.picbx_record.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_record.Image = global::Cam_Watcher.Properties.Resources.btn_play;
            this.picbx_record.Location = new System.Drawing.Point(749, 575);
            this.picbx_record.Name = "picbx_record";
            this.picbx_record.Size = new System.Drawing.Size(70, 70);
            this.picbx_record.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_record.TabIndex = 12;
            this.picbx_record.TabStop = false;
            this.picbx_record.Click += new System.EventHandler(this.picbx_record_Click);
            // 
            // picbx_main_webcam
            // 
            this.picbx_main_webcam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picbx_main_webcam.Location = new System.Drawing.Point(283, 44);
            this.picbx_main_webcam.Name = "picbx_main_webcam";
            this.picbx_main_webcam.Size = new System.Drawing.Size(700, 525);
            this.picbx_main_webcam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picbx_main_webcam.TabIndex = 5;
            this.picbx_main_webcam.TabStop = false;
            // 
            // grpbx_recording
            // 
            this.grpbx_recording.Controls.Add(this.rdbtn_motion);
            this.grpbx_recording.Controls.Add(this.rdbtn_traditional);
            this.grpbx_recording.Controls.Add(this.label1);
            this.grpbx_recording.Controls.Add(this.numud_duration);
            this.grpbx_recording.Controls.Add(this.labl_duration);
            this.grpbx_recording.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpbx_recording.Location = new System.Drawing.Point(12, 570);
            this.grpbx_recording.Name = "grpbx_recording";
            this.grpbx_recording.Size = new System.Drawing.Size(440, 70);
            this.grpbx_recording.TabIndex = 22;
            this.grpbx_recording.TabStop = false;
            this.grpbx_recording.Text = "Recording";
            // 
            // rdbtn_motion
            // 
            this.rdbtn_motion.AutoSize = true;
            this.rdbtn_motion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdbtn_motion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbtn_motion.Location = new System.Drawing.Point(284, 40);
            this.rdbtn_motion.Name = "rdbtn_motion";
            this.rdbtn_motion.Size = new System.Drawing.Size(129, 25);
            this.rdbtn_motion.TabIndex = 13;
            this.rdbtn_motion.Text = "Motion Sensor";
            this.rdbtn_motion.UseVisualStyleBackColor = true;
            // 
            // rdbtn_traditional
            // 
            this.rdbtn_traditional.AutoSize = true;
            this.rdbtn_traditional.Checked = true;
            this.rdbtn_traditional.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdbtn_traditional.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbtn_traditional.Location = new System.Drawing.Point(285, 16);
            this.rdbtn_traditional.Name = "rdbtn_traditional";
            this.rdbtn_traditional.Size = new System.Drawing.Size(100, 25);
            this.rdbtn_traditional.TabIndex = 12;
            this.rdbtn_traditional.TabStop = true;
            this.rdbtn_traditional.Text = "Traditional";
            this.rdbtn_traditional.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(211, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 21);
            this.label1.TabIndex = 11;
            this.label1.Text = "Method:";
            // 
            // numud_duration
            // 
            this.numud_duration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.numud_duration.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numud_duration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numud_duration.ForeColor = System.Drawing.Color.White;
            this.numud_duration.Location = new System.Drawing.Point(97, 30);
            this.numud_duration.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numud_duration.Name = "numud_duration";
            this.numud_duration.Size = new System.Drawing.Size(50, 25);
            this.numud_duration.TabIndex = 10;
            this.numud_duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numud_duration.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // labl_duration
            // 
            this.labl_duration.AutoSize = true;
            this.labl_duration.Location = new System.Drawing.Point(20, 30);
            this.labl_duration.Name = "labl_duration";
            this.labl_duration.Size = new System.Drawing.Size(151, 21);
            this.labl_duration.TabIndex = 9;
            this.labl_duration.Text = "Duration:                 h";
            // 
            // timer_server
            // 
            this.timer_server.Interval = 1000;
            this.timer_server.Tick += new System.EventHandler(this.timer_server_Tick);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.ClientSize = new System.Drawing.Size(994, 651);
            this.Controls.Add(this.grpbx_recording);
            this.Controls.Add(this.buttn_settings);
            this.Controls.Add(this.labl_time_elapsed);
            this.Controls.Add(this.picbx_spinner);
            this.Controls.Add(this.picbx_record);
            this.Controls.Add(this.labl_main_webcam);
            this.Controls.Add(this.picbx_main_webcam);
            this.Controls.Add(this.flpnl_list_webcam);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(1010, 690);
            this.MinimumSize = new System.Drawing.Size(1010, 690);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cam Watcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Home_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picbx_spinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_record)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_main_webcam)).EndInit();
            this.grpbx_recording.ResumeLayout(false);
            this.grpbx_recording.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numud_duration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpnl_list_webcam;
        public System.Windows.Forms.PictureBox picbx_main_webcam;
        public System.Windows.Forms.Label labl_main_webcam;
        private System.Windows.Forms.PictureBox picbx_record;
        private System.Windows.Forms.PictureBox picbx_spinner;
        private System.Windows.Forms.Label labl_time_elapsed;
        private System.Windows.Forms.Button buttn_settings;
        private System.Windows.Forms.GroupBox grpbx_recording;
        private System.Windows.Forms.NumericUpDown numud_duration;
        private System.Windows.Forms.Label labl_duration;
        private System.Windows.Forms.RadioButton rdbtn_motion;
        private System.Windows.Forms.RadioButton rdbtn_traditional;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Timer timer_record;
        public System.Windows.Forms.Timer timer_server;
    }
}