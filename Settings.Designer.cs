﻿namespace Cam_Watcher
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.labl_title = new System.Windows.Forms.Label();
            this.picbx_line1 = new System.Windows.Forms.PictureBox();
            this.grpbx_server = new System.Windows.Forms.GroupBox();
            this.txtbx_server_url = new System.Windows.Forms.TextBox();
            this.labl_server_url = new System.Windows.Forms.Label();
            this.cmbbx_server_ip = new System.Windows.Forms.ComboBox();
            this.labl_server_ip = new System.Windows.Forms.Label();
            this.labl_server_link = new System.Windows.Forms.Label();
            this.txtbx_server_path = new System.Windows.Forms.TextBox();
            this.labl_server_path = new System.Windows.Forms.Label();
            this.grpbx_recording = new System.Windows.Forms.GroupBox();
            this.numud_recording_motion_sensibility = new System.Windows.Forms.NumericUpDown();
            this.labl_recording_motion_sensibility = new System.Windows.Forms.Label();
            this.txtbx_recording_path = new System.Windows.Forms.TextBox();
            this.labl_recording_path = new System.Windows.Forms.Label();
            this.numud_recording_snaps_interval = new System.Windows.Forms.NumericUpDown();
            this.labl_recording_snaps_interval = new System.Windows.Forms.Label();
            this.buttn_save = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_line1)).BeginInit();
            this.grpbx_server.SuspendLayout();
            this.grpbx_recording.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numud_recording_motion_sensibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numud_recording_snaps_interval)).BeginInit();
            this.SuspendLayout();
            // 
            // labl_title
            // 
            this.labl_title.AutoSize = true;
            this.labl_title.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labl_title.ForeColor = System.Drawing.Color.White;
            this.labl_title.Location = new System.Drawing.Point(141, 3);
            this.labl_title.Name = "labl_title";
            this.labl_title.Size = new System.Drawing.Size(101, 32);
            this.labl_title.TabIndex = 2;
            this.labl_title.Text = "Settings";
            this.labl_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picbx_line1
            // 
            this.picbx_line1.BackColor = System.Drawing.Color.White;
            this.picbx_line1.Location = new System.Drawing.Point(22, 20);
            this.picbx_line1.Name = "picbx_line1";
            this.picbx_line1.Size = new System.Drawing.Size(340, 4);
            this.picbx_line1.TabIndex = 1;
            this.picbx_line1.TabStop = false;
            // 
            // grpbx_server
            // 
            this.grpbx_server.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.grpbx_server.Controls.Add(this.txtbx_server_url);
            this.grpbx_server.Controls.Add(this.labl_server_url);
            this.grpbx_server.Controls.Add(this.cmbbx_server_ip);
            this.grpbx_server.Controls.Add(this.labl_server_ip);
            this.grpbx_server.Controls.Add(this.labl_server_link);
            this.grpbx_server.Controls.Add(this.txtbx_server_path);
            this.grpbx_server.Controls.Add(this.labl_server_path);
            this.grpbx_server.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpbx_server.Location = new System.Drawing.Point(22, 50);
            this.grpbx_server.Name = "grpbx_server";
            this.grpbx_server.Size = new System.Drawing.Size(340, 165);
            this.grpbx_server.TabIndex = 15;
            this.grpbx_server.TabStop = false;
            this.grpbx_server.Text = "Server";
            // 
            // txtbx_server_url
            // 
            this.txtbx_server_url.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.txtbx_server_url.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_server_url.ForeColor = System.Drawing.Color.White;
            this.txtbx_server_url.Location = new System.Drawing.Point(66, 90);
            this.txtbx_server_url.MaxLength = 255;
            this.txtbx_server_url.Name = "txtbx_server_url";
            this.txtbx_server_url.Size = new System.Drawing.Size(250, 22);
            this.txtbx_server_url.TabIndex = 11;
            // 
            // labl_server_url
            // 
            this.labl_server_url.AutoSize = true;
            this.labl_server_url.Location = new System.Drawing.Point(20, 90);
            this.labl_server_url.Name = "labl_server_url";
            this.labl_server_url.Size = new System.Drawing.Size(43, 21);
            this.labl_server_url.TabIndex = 10;
            this.labl_server_url.Text = "Path:";
            // 
            // cmbbx_server_ip
            // 
            this.cmbbx_server_ip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.cmbbx_server_ip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbbx_server_ip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbbx_server_ip.ForeColor = System.Drawing.Color.White;
            this.cmbbx_server_ip.FormattingEnabled = true;
            this.cmbbx_server_ip.Location = new System.Drawing.Point(113, 56);
            this.cmbbx_server_ip.Name = "cmbbx_server_ip";
            this.cmbbx_server_ip.Size = new System.Drawing.Size(200, 29);
            this.cmbbx_server_ip.TabIndex = 9;
            // 
            // labl_server_ip
            // 
            this.labl_server_ip.AutoSize = true;
            this.labl_server_ip.Location = new System.Drawing.Point(20, 60);
            this.labl_server_ip.Name = "labl_server_ip";
            this.labl_server_ip.Size = new System.Drawing.Size(86, 21);
            this.labl_server_ip.TabIndex = 8;
            this.labl_server_ip.Text = "IP Address:";
            // 
            // labl_server_link
            // 
            this.labl_server_link.AutoSize = true;
            this.labl_server_link.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labl_server_link.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(206)))), ((int)(((byte)(120)))));
            this.labl_server_link.Location = new System.Drawing.Point(20, 120);
            this.labl_server_link.Name = "labl_server_link";
            this.labl_server_link.Size = new System.Drawing.Size(101, 21);
            this.labl_server_link.TabIndex = 7;
            this.labl_server_link.Text = "View website";
            this.labl_server_link.Click += new System.EventHandler(this.labl_server_link_Click);
            // 
            // txtbx_server_path
            // 
            this.txtbx_server_path.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.txtbx_server_path.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_server_path.ForeColor = System.Drawing.Color.White;
            this.txtbx_server_path.Location = new System.Drawing.Point(66, 30);
            this.txtbx_server_path.MaxLength = 255;
            this.txtbx_server_path.Name = "txtbx_server_path";
            this.txtbx_server_path.Size = new System.Drawing.Size(250, 22);
            this.txtbx_server_path.TabIndex = 6;
            // 
            // labl_server_path
            // 
            this.labl_server_path.AutoSize = true;
            this.labl_server_path.Location = new System.Drawing.Point(20, 30);
            this.labl_server_path.Name = "labl_server_path";
            this.labl_server_path.Size = new System.Drawing.Size(43, 21);
            this.labl_server_path.TabIndex = 5;
            this.labl_server_path.Text = "Path:";
            // 
            // grpbx_recording
            // 
            this.grpbx_recording.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.grpbx_recording.Controls.Add(this.numud_recording_motion_sensibility);
            this.grpbx_recording.Controls.Add(this.labl_recording_motion_sensibility);
            this.grpbx_recording.Controls.Add(this.txtbx_recording_path);
            this.grpbx_recording.Controls.Add(this.labl_recording_path);
            this.grpbx_recording.Controls.Add(this.numud_recording_snaps_interval);
            this.grpbx_recording.Controls.Add(this.labl_recording_snaps_interval);
            this.grpbx_recording.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpbx_recording.Location = new System.Drawing.Point(22, 236);
            this.grpbx_recording.Name = "grpbx_recording";
            this.grpbx_recording.Size = new System.Drawing.Size(340, 135);
            this.grpbx_recording.TabIndex = 16;
            this.grpbx_recording.TabStop = false;
            this.grpbx_recording.Text = "Recording";
            // 
            // numud_recording_motion_sensibility
            // 
            this.numud_recording_motion_sensibility.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.numud_recording_motion_sensibility.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numud_recording_motion_sensibility.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numud_recording_motion_sensibility.ForeColor = System.Drawing.Color.White;
            this.numud_recording_motion_sensibility.Location = new System.Drawing.Point(160, 89);
            this.numud_recording_motion_sensibility.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numud_recording_motion_sensibility.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numud_recording_motion_sensibility.Name = "numud_recording_motion_sensibility";
            this.numud_recording_motion_sensibility.Size = new System.Drawing.Size(80, 25);
            this.numud_recording_motion_sensibility.TabIndex = 17;
            this.numud_recording_motion_sensibility.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numud_recording_motion_sensibility.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labl_recording_motion_sensibility
            // 
            this.labl_recording_motion_sensibility.AutoSize = true;
            this.labl_recording_motion_sensibility.Location = new System.Drawing.Point(20, 90);
            this.labl_recording_motion_sensibility.Name = "labl_recording_motion_sensibility";
            this.labl_recording_motion_sensibility.Size = new System.Drawing.Size(136, 21);
            this.labl_recording_motion_sensibility.TabIndex = 16;
            this.labl_recording_motion_sensibility.Text = "Motion sensibility:";
            // 
            // txtbx_recording_path
            // 
            this.txtbx_recording_path.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.txtbx_recording_path.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_recording_path.ForeColor = System.Drawing.Color.White;
            this.txtbx_recording_path.Location = new System.Drawing.Point(66, 30);
            this.txtbx_recording_path.MaxLength = 255;
            this.txtbx_recording_path.Name = "txtbx_recording_path";
            this.txtbx_recording_path.Size = new System.Drawing.Size(250, 22);
            this.txtbx_recording_path.TabIndex = 9;
            // 
            // labl_recording_path
            // 
            this.labl_recording_path.AutoSize = true;
            this.labl_recording_path.Location = new System.Drawing.Point(20, 30);
            this.labl_recording_path.Name = "labl_recording_path";
            this.labl_recording_path.Size = new System.Drawing.Size(43, 21);
            this.labl_recording_path.TabIndex = 8;
            this.labl_recording_path.Text = "Path:";
            // 
            // numud_recording_snaps_interval
            // 
            this.numud_recording_snaps_interval.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.numud_recording_snaps_interval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numud_recording_snaps_interval.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numud_recording_snaps_interval.ForeColor = System.Drawing.Color.White;
            this.numud_recording_snaps_interval.Location = new System.Drawing.Point(159, 59);
            this.numud_recording_snaps_interval.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numud_recording_snaps_interval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numud_recording_snaps_interval.Name = "numud_recording_snaps_interval";
            this.numud_recording_snaps_interval.Size = new System.Drawing.Size(50, 25);
            this.numud_recording_snaps_interval.TabIndex = 15;
            this.numud_recording_snaps_interval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numud_recording_snaps_interval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labl_recording_snaps_interval
            // 
            this.labl_recording_snaps_interval.AutoSize = true;
            this.labl_recording_snaps_interval.Location = new System.Drawing.Point(20, 60);
            this.labl_recording_snaps_interval.Name = "labl_recording_snaps_interval";
            this.labl_recording_snaps_interval.Size = new System.Drawing.Size(214, 21);
            this.labl_recording_snaps_interval.TabIndex = 14;
            this.labl_recording_snaps_interval.Text = "Interval snapshots:                 s";
            // 
            // buttn_save
            // 
            this.buttn_save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttn_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttn_save.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(206)))), ((int)(((byte)(120)))));
            this.buttn_save.Location = new System.Drawing.Point(132, 396);
            this.buttn_save.Name = "buttn_save";
            this.buttn_save.Size = new System.Drawing.Size(110, 40);
            this.buttn_save.TabIndex = 17;
            this.buttn_save.Text = "Save";
            this.buttn_save.UseVisualStyleBackColor = true;
            this.buttn_save.Click += new System.EventHandler(this.buttn_save_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.buttn_save);
            this.Controls.Add(this.grpbx_recording);
            this.Controls.Add(this.grpbx_server);
            this.Controls.Add(this.labl_title);
            this.Controls.Add(this.picbx_line1);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(400, 500);
            this.MinimumSize = new System.Drawing.Size(400, 500);
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cam Watcher";
            ((System.ComponentModel.ISupportInitialize)(this.picbx_line1)).EndInit();
            this.grpbx_server.ResumeLayout(false);
            this.grpbx_server.PerformLayout();
            this.grpbx_recording.ResumeLayout(false);
            this.grpbx_recording.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numud_recording_motion_sensibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numud_recording_snaps_interval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox picbx_line1;
        private System.Windows.Forms.Label labl_title;
        private System.Windows.Forms.GroupBox grpbx_server;
        private System.Windows.Forms.Label labl_server_link;
        private System.Windows.Forms.TextBox txtbx_server_path;
        private System.Windows.Forms.Label labl_server_path;
        private System.Windows.Forms.GroupBox grpbx_recording;
        private System.Windows.Forms.TextBox txtbx_recording_path;
        private System.Windows.Forms.Label labl_recording_path;
        private System.Windows.Forms.NumericUpDown numud_recording_snaps_interval;
        private System.Windows.Forms.Label labl_recording_snaps_interval;
        private System.Windows.Forms.Button buttn_save;
        private System.Windows.Forms.Label labl_recording_motion_sensibility;
        private System.Windows.Forms.TextBox txtbx_server_url;
        private System.Windows.Forms.Label labl_server_url;
        private System.Windows.Forms.ComboBox cmbbx_server_ip;
        private System.Windows.Forms.Label labl_server_ip;
        private System.Windows.Forms.NumericUpDown numud_recording_motion_sensibility;
    }
}