﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video.DirectShow;

namespace Cam_Watcher
{
    public partial class CamItem : UserControl
    {
        public FilterInfo webcamInfo;
        public bool enabled;

        public CamItem(FilterInfo wci)
        {
            InitializeComponent();
            webcamInfo = wci; enabled = false;
            labl_name.Text = webcamInfo.Name;
        }

        private void picbx_image_Click(object sender, EventArgs e)
        {
            control_click();
        }

        private void labl_name_Click(object sender, EventArgs e)
        {
            control_click();
        }

        private void control_click()
        {
            if (enabled)
            {
                picbx_image.Image = Properties.Resources.camera_disabled;
                labl_name.ForeColor = Color.White;
            }
            else
            {
                picbx_image.Image = Properties.Resources.camera_enabled;
                labl_name.ForeColor = Color.FromArgb(254, 206, 120);
            }
            enabled = !enabled;
        }
    }
}
