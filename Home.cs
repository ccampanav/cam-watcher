﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using System.IO;
using System.Net;

namespace Cam_Watcher
{
    public partial class Home : Form
    {
        Settings frmSettings;
        public List<FilterInfo> webcamsList;
        bool recording;
        Int64 valRecord, valLimit, valCaptures;
        int timeHour, timeMin, timeSec, camsCount;
        public char recordingMethod;
        //Parameters List
        public string paramPathServer;
        public string paramIPServer;
        public string paramURLServer;
        public string paramPathSnapshots;
        public int paramSnapshotsInterval;
        public int paramMotionSensibility;

        public Home(List<FilterInfo> wcList)
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            webcamsList = wcList; recording = false;
            camsCount = 0; recordingMethod = '-';
            foreach (FilterInfo wc in webcamsList)
            {
                camsCount++; flpnl_list_webcam.Controls.Add(new Webcam(this, wc, camsCount));
            }
            if (get_configfile(6))
            {
                int idxIP = get_index_ip();
                if (idxIP == -1)
                {
                    show_message("Select another IP in Settings", 0);
                }
                File.WriteAllText(paramPathServer + "/index.html", generate_html_file(camsCount));

                foreach (Webcam wc in flpnl_list_webcam.Controls)
                {
                    wc.start();
                }
                set_main_webcam(webcamsList[0].Name);
                timer_server.Enabled = true;
            }
            else
            {
                this.Enabled = false;
            }
        }

        public void set_main_webcam(string name)
        {
            foreach (Webcam wc in flpnl_list_webcam.Controls)
            {
                wc.isMain = false;
                wc.BackColor = Color.FromArgb(49, 48, 53);
            }
            foreach (Webcam wc in flpnl_list_webcam.Controls)
            {
                if (wc.get_name().Equals(name))
                {
                    wc.isMain = true;
                    labl_main_webcam.Text = name;
                    wc.BackColor = Color.FromArgb(254, 206, 120);
                    break;
                }
            }
        }

        private void picbx_record_Click(object sender, EventArgs e)
        {
            if (recording) //Stop
            {
                timer_stop();
                picbx_record.Image = Properties.Resources.btn_play;
            }
            else //Play
            {
                numud_duration.Enabled = false;
                rdbtn_motion.Enabled = false; rdbtn_traditional.Enabled = false;
                timeHour = 0; timeMin = 0; timeSec = 0; valCaptures = 0;
                valRecord = 0; valLimit = (int)(numud_duration.Value * 60 * 60);
                //valLimit = 10; //Line for testing
                if (rdbtn_traditional.Checked)
                {
                    recordingMethod = 't';
                }
                else
                {
                    recordingMethod = 'm';
                }
                picbx_spinner.Visible = true; timer_record.Enabled = true;
                picbx_record.Image = Properties.Resources.btn_stop;
            }
            recording = !recording;           
        }

        private void timer_record_Tick(object sender, EventArgs e)
        {
            if (valRecord < valLimit)
            {
                valRecord++;        
                if (recordingMethod == 't' && valRecord % paramSnapshotsInterval == 0)
                {
                    foreach (Webcam wc in flpnl_list_webcam.Controls)
                    {
                        wc.snapshot(generate_snapshot_format());
                    }
                }
                labl_time_elapsed.Text = generate_time_format(valRecord);               
            }
            else
            {
                timer_stop(); recording = false;
                picbx_record.Image = Properties.Resources.btn_play;
            }           
        }

        private string generate_time_format(Int64 s)
        {
            string answ = "";
            timeHour = (int)Math.Floor((decimal)(s / 3600)); s -= timeHour * 3600;
            timeMin = (int)Math.Floor((decimal)(s / 60)); s -= timeMin * 60;
            timeSec = (int)s;
            if (timeHour < 10)
            {
                answ += "0";
            }
            answ += timeHour + ":";
            if (timeMin < 10)
            {
                answ += "0";
            }
            answ += timeMin + ":";
            if (timeSec < 10)
            {
                answ += "0";
            }
            answ += timeSec;
            return answ;
        }

        public string generate_snapshot_format()
        {
            DateTime dtNow = DateTime.Now;
            string answ, year, month, day, hour, min, sec;
            answ = "" + dtNow.Year.ToString().Substring(2, 2);
            if (dtNow.Month < 10)
            {
                answ += "0";
            }
            answ += dtNow.Month;
            if (dtNow.Day < 10)
            {
                answ += "0";
            }
            answ += dtNow.Day + "-";
            if (dtNow.Hour < 10)
            {
                answ += "0";
            }
            answ += dtNow.Hour;
            if (dtNow.Minute < 10)
            {
                answ += "0";
            }
            answ += dtNow.Minute;
            if (dtNow.Second < 10)
            {
                answ += "0";
            }
            answ += dtNow.Second;
            return answ;
        }

        private void buttn_settings_Click(object sender, EventArgs e)
        {
            frmSettings = new Settings(this);
            frmSettings.Show();
        }

        private void timer_stop()
        {
            numud_duration.Enabled = true;
            rdbtn_motion.Enabled = true; rdbtn_traditional.Enabled = true;
            picbx_spinner.Visible = false; timer_record.Enabled = false;
        }

        public bool get_configfile(int configLength)
        {
            bool answ = false;
            List<string> data = new List<string>();
            if (File.Exists("config.txt"))
            {
                string[] configData = File.ReadAllLines("config.txt");
                if (configData.Length == configLength)
                {
                    if (Directory.Exists(configData[0])) //Check ServerPath
                    {
                        data.Add(configData[0]);
                    } 
                    string[] auxIP = configData[1].Split('.');
                    if (auxIP.Length == 4) //Check ServerIP
                    {
                        data.Add(configData[1]);
                    }
                    string[] auxURL = configData[2].Split(' ');
                    if (auxURL.Length == 1) //Check ServerURL
                    {
                        data.Add(configData[2]);
                    }
                    if (Directory.Exists(configData[3])) //Check SnapshotsPath
                    {
                        data.Add(configData[3]);
                    }
                    try
                    {
                        int intAux;
                        intAux = int.Parse(configData[4]); //Check SnapsInterval
                        if (intAux >= 1 && intAux <= 60)
                        {
                            data.Add(configData[4]);
                        }
                        intAux = int.Parse(configData[5]); //Check MotionSensibility
                        if (intAux >= 1 && intAux <= 999999)
                        {
                            data.Add(configData[5]);
                        }
                    }
                    catch (Exception e) { }
                    if (data.Count == configLength)
                    {
                        set_parameters(data); answ = true;
                    }
                    else
                    {
                        show_message("Configuration file is corrupted", 1);
                    }
                }
            }
            return answ;
        }

        public void save_configfile(List<string> data)
        {
            string strData = "";
            for (int i = 0; i < data.Count - 1; i++)
            {
                strData += data[i] + Environment.NewLine;
            }
            strData += data[data.Count - 1];
            File.WriteAllText("config.txt", strData);
            set_parameters(data);
        }

        private void set_parameters(List<string> data)
        {
            paramPathServer = data[0];
            paramIPServer = data[1];
            paramURLServer = data[2];
            paramPathSnapshots = data[3];
            paramSnapshotsInterval = int.Parse(data[4]);
            paramMotionSensibility = int.Parse(data[5]);
        }

        public int get_index_ip()
        {
            int answ = -1;
            string hostName = Dns.GetHostName();
            for (int k = 0; k < Dns.GetHostByName(hostName).AddressList.Count(); k++)
            {
                if (Dns.GetHostByName(hostName).AddressList[k].ToString().Equals(paramIPServer))
                {
                    answ = k;
                }
            }
             return answ;
        }

        private void timer_server_Tick(object sender, EventArgs e)
        {
            foreach (Webcam wc in flpnl_list_webcam.Controls)
            {
                wc.image_server();
            }
        }

        public void show_message(string msg, int type)
        {
            switch (type)
            {
                case 0:
                    MessageBox.Show(msg, "Cam Watcher", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case 1:
                    MessageBox.Show(msg, "Cam Watcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private string generate_html_file(int cams)
        {
            string data = "<html><head><title>Cam Monitor</title><style>h1{color: #FECE78;font-family: \"Segoe UI\"; font-size: 40;}</style></head>";
            data += "<body bgcolor=\"#313035\"><center><h1>CAM MONITOR</h1><hr width=\"70%\"><br><br>";
            for (int c = 1; c <= cams; c++)
            {
                data += "<img src=\"cam" + c + ".png\" width=\"70%\">";
            }
            data += "</center></body></html>";
            return data;
        }

        private void Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Webcam wc in flpnl_list_webcam.Controls)
            {
                wc.stop();
            }
            Application.Exit();
        }
    }
}
