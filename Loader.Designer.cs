﻿namespace Cam_Watcher
{
    partial class Loader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Loader));
            this.labl_webcams = new System.Windows.Forms.Label();
            this.flpnl_webcams = new System.Windows.Forms.FlowLayoutPanel();
            this.buttn_continue = new System.Windows.Forms.Button();
            this.picbx_line = new System.Windows.Forms.PictureBox();
            this.picbx_wallpaper = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_line)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_wallpaper)).BeginInit();
            this.SuspendLayout();
            // 
            // labl_webcams
            // 
            this.labl_webcams.AutoSize = true;
            this.labl_webcams.Location = new System.Drawing.Point(164, 330);
            this.labl_webcams.Name = "labl_webcams";
            this.labl_webcams.Size = new System.Drawing.Size(156, 21);
            this.labl_webcams.TabIndex = 2;
            this.labl_webcams.Text = "Select your webcams";
            // 
            // flpnl_webcams
            // 
            this.flpnl_webcams.AutoScroll = true;
            this.flpnl_webcams.Location = new System.Drawing.Point(43, 365);
            this.flpnl_webcams.Name = "flpnl_webcams";
            this.flpnl_webcams.Size = new System.Drawing.Size(400, 180);
            this.flpnl_webcams.TabIndex = 3;
            // 
            // buttn_continue
            // 
            this.buttn_continue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttn_continue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttn_continue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(206)))), ((int)(((byte)(120)))));
            this.buttn_continue.Location = new System.Drawing.Point(188, 555);
            this.buttn_continue.Name = "buttn_continue";
            this.buttn_continue.Size = new System.Drawing.Size(110, 40);
            this.buttn_continue.TabIndex = 5;
            this.buttn_continue.Text = "Continue";
            this.buttn_continue.UseVisualStyleBackColor = true;
            this.buttn_continue.Click += new System.EventHandler(this.buttn_continue_Click);
            // 
            // picbx_line
            // 
            this.picbx_line.BackColor = System.Drawing.Color.White;
            this.picbx_line.Location = new System.Drawing.Point(43, 310);
            this.picbx_line.Name = "picbx_line";
            this.picbx_line.Size = new System.Drawing.Size(400, 4);
            this.picbx_line.TabIndex = 1;
            this.picbx_line.TabStop = false;
            // 
            // picbx_wallpaper
            // 
            this.picbx_wallpaper.Image = global::Cam_Watcher.Properties.Resources.Wallpaper;
            this.picbx_wallpaper.Location = new System.Drawing.Point(93, 0);
            this.picbx_wallpaper.Name = "picbx_wallpaper";
            this.picbx_wallpaper.Size = new System.Drawing.Size(300, 300);
            this.picbx_wallpaper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_wallpaper.TabIndex = 0;
            this.picbx_wallpaper.TabStop = false;
            // 
            // Loader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.ClientSize = new System.Drawing.Size(484, 611);
            this.Controls.Add(this.buttn_continue);
            this.Controls.Add(this.flpnl_webcams);
            this.Controls.Add(this.labl_webcams);
            this.Controls.Add(this.picbx_line);
            this.Controls.Add(this.picbx_wallpaper);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(500, 650);
            this.MinimumSize = new System.Drawing.Size(500, 650);
            this.Name = "Loader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cam Watcher";
            ((System.ComponentModel.ISupportInitialize)(this.picbx_line)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_wallpaper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picbx_wallpaper;
        private System.Windows.Forms.PictureBox picbx_line;
        private System.Windows.Forms.Label labl_webcams;
        private System.Windows.Forms.FlowLayoutPanel flpnl_webcams;
        private System.Windows.Forms.Button buttn_continue;
    }
}

