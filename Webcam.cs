﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;
using AForge.Vision.Motion;

namespace Cam_Watcher
{
    public partial class Webcam : UserControl
    {
        Home frmHome;
        VideoCaptureDevice vcdItem;
        MotionDetector mtnDetector;
        FilterInfo webcamInfo;
        Bitmap image, imageSnap, imageServer, imageMotion;
        int index, valMotion;
        public Int64 movements;
        public bool isMain;

        public Webcam(Home fHome, FilterInfo wci, int idx)
        {
            InitializeComponent();
            frmHome = fHome;
            webcamInfo = wci; index = idx;
            mtnDetector = new MotionDetector(new TwoFramesDifferenceDetector(), new MotionAreaHighlighting());
            vcdItem = new VideoCaptureDevice(webcamInfo.MonikerString);
            isMain = false; valMotion = 0;
            vcdItem.NewFrame += VideoCaptureDevice_NewFrame;            
        }

        private void VideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            image = (Bitmap)eventArgs.Frame.Clone();
            imageSnap = (Bitmap)eventArgs.Frame.Clone();
            imageServer = (Bitmap)eventArgs.Frame.Clone();
            imageMotion = (Bitmap)eventArgs.Frame.Clone();
            try
            {
                valMotion = (int)(mtnDetector.ProcessFrame(imageMotion) * 100000);
                picbx_webcam.Image = imageMotion;
            }
            catch (Exception e) { }   
            if (frmHome.timer_record.Enabled)
            {
                if (frmHome.recordingMethod == 'm')
                {
                    if (valMotion >= frmHome.paramMotionSensibility)
                    {
                        snapshot(frmHome.generate_snapshot_format());
                    }
                }
            }
            if (isMain)
            {
                try
                {
                    frmHome.picbx_main_webcam.Image = imageMotion;                  
                } catch (Exception e) { }   
            }
            Console.WriteLine(webcamInfo.Name + ":" + valMotion);
        }

        public void start()
        {
            movements = 0; vcdItem.Start();
        }

        public void stop()
        {
            vcdItem.Stop();
        }

        public string get_name()
        {
            return webcamInfo.Name;
        }

        public void snapshot(string fname)
        {
            //try{
                imageSnap.Save(frmHome.paramPathSnapshots + "/" + index + "_" + fname + ".png", ImageFormat.Png);
            //} catch (Exception e) {  }
        }

        public void image_server()
        {
            //try {
                imageServer.Save(frmHome.paramPathServer + "/cam" + index + ".png", ImageFormat.Png);
            //} catch (Exception e) { }
        }

        private void picbx_webcam_Click(object sender, EventArgs e)
        {
            frmHome.set_main_webcam(webcamInfo.Name);
        }
    }
}
