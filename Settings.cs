﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace Cam_Watcher
{
    public partial class Settings : Form
    {
        Home frmHome;

        public Settings(Home fHome)
        {
            InitializeComponent();
            frmHome = fHome;
            txtbx_server_path.Text = frmHome.paramPathServer;
            get_list_ip();
            int idxIP = frmHome.get_index_ip();
            if (idxIP >= 0)
            {
                cmbbx_server_ip.SelectedIndex = idxIP;
            }
            txtbx_server_url.Text = frmHome.paramURLServer;
            txtbx_recording_path.Text = frmHome.paramPathSnapshots;
            numud_recording_snaps_interval.Value = (decimal)frmHome.paramSnapshotsInterval;
            numud_recording_motion_sensibility.Value = (decimal)frmHome.paramMotionSensibility;

            labl_title.Select();
        }

        private void get_list_ip()
        {
            string hostName = Dns.GetHostName();
            string auxIP;
            for (int k = 0; k < Dns.GetHostByName(hostName).AddressList.Count(); k++)
            {
                auxIP = Dns.GetHostByName(hostName).AddressList[k].ToString();
                cmbbx_server_ip.Items.Add(auxIP);
            }
        }

        private void buttn_save_Click(object sender, EventArgs e)
        {
            List<string> data = new List<string>();
            int aux = 0;
            if (Directory.Exists(txtbx_server_path.Text))
            {
                aux++; data.Add(txtbx_server_path.Text);
            }
            string[] auxIP = cmbbx_server_ip.SelectedItem.ToString().Split('.');
            if (auxIP.Length == 4)
            {
                aux++; data.Add(cmbbx_server_ip.SelectedItem.ToString());
            }
            string[] auxURL = txtbx_server_url.Text.Split(' ');
            if (auxURL.Length == 1)
            {
                aux++; data.Add(txtbx_server_url.Text);
            }
            if (Directory.Exists(txtbx_recording_path.Text))
            {
                aux++; data.Add(txtbx_recording_path.Text);
            }
            aux++; data.Add(((int)numud_recording_snaps_interval.Value).ToString());
            aux++; data.Add(((int)numud_recording_motion_sensibility.Value).ToString());
            if (aux == 6)
            {
                frmHome.save_configfile(data);
                this.Close();
            }
            else
            {
                frmHome.show_message("Configuration is corrupted", 1);
            }
        }

        private void labl_server_link_Click(object sender, EventArgs e)
        {
            Process.Start("http://" + frmHome.paramIPServer + frmHome.paramURLServer +
                "index.html");
        }
    }
}
