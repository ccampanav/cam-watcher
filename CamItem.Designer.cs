﻿namespace Cam_Watcher
{
    partial class CamItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labl_name = new System.Windows.Forms.Label();
            this.picbx_image = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_image)).BeginInit();
            this.SuspendLayout();
            // 
            // labl_name
            // 
            this.labl_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labl_name.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labl_name.Location = new System.Drawing.Point(48, 7);
            this.labl_name.Name = "labl_name";
            this.labl_name.Size = new System.Drawing.Size(320, 35);
            this.labl_name.TabIndex = 1;
            this.labl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labl_name.Click += new System.EventHandler(this.labl_name_Click);
            // 
            // picbx_image
            // 
            this.picbx_image.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_image.Image = global::Cam_Watcher.Properties.Resources.camera_disabled;
            this.picbx_image.Location = new System.Drawing.Point(5, 5);
            this.picbx_image.Name = "picbx_image";
            this.picbx_image.Size = new System.Drawing.Size(40, 40);
            this.picbx_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_image.TabIndex = 0;
            this.picbx_image.TabStop = false;
            this.picbx_image.Click += new System.EventHandler(this.picbx_image_Click);
            // 
            // CamItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.Controls.Add(this.labl_name);
            this.Controls.Add(this.picbx_image);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CamItem";
            this.Size = new System.Drawing.Size(375, 50);
            ((System.ComponentModel.ISupportInitialize)(this.picbx_image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picbx_image;
        private System.Windows.Forms.Label labl_name;
    }
}
