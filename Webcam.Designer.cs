﻿namespace Cam_Watcher
{
    partial class Webcam
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picbx_webcam = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_webcam)).BeginInit();
            this.SuspendLayout();
            // 
            // picbx_webcam
            // 
            this.picbx_webcam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.picbx_webcam.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_webcam.Location = new System.Drawing.Point(5, 5);
            this.picbx_webcam.Name = "picbx_webcam";
            this.picbx_webcam.Size = new System.Drawing.Size(230, 170);
            this.picbx_webcam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picbx_webcam.TabIndex = 0;
            this.picbx_webcam.TabStop = false;
            this.picbx_webcam.Click += new System.EventHandler(this.picbx_webcam_Click);
            // 
            // Webcam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(48)))), ((int)(((byte)(53)))));
            this.Controls.Add(this.picbx_webcam);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Webcam";
            this.Size = new System.Drawing.Size(240, 180);
            ((System.ComponentModel.ISupportInitialize)(this.picbx_webcam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picbx_webcam;
    }
}
